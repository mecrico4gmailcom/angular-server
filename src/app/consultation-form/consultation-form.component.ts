import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ConsultationService } from "../consultation.service";
import { DoctorService } from "../doctor.service";
import { PatientService } from "../patient.service";
import { Consultation } from "../models/consultation";
import { Doctor } from "../models/doctor";
import { Patient } from "../models/patient";

@Component({
	selector: 'app-consultation-form',
	templateUrl: './consultation-form.component.html',
	styleUrls: ['./consultation-form.component.scss']
})
export class ConsultationFormComponent implements OnInit {

	@Input() source?: string;
	@Input() selectedDoctor?: Doctor;
	@Input() selectedPatient?: Patient;
	consultation: Consultation;
	doctors: Doctor[]  = [];
	patients: Patient[]  = [];
	constructor(
		private consultationService: ConsultationService,
		private doctorService: DoctorService,
		private patientService: PatientService,
		private location: Location
	) { }

	ngOnInit(): void {
		if( this.source != "patient"){
			this.getPatients();
		}
		if( this.source != "doctor"){
			this.getDoctors();
		}
	}

	getDoctors(): void {
		this.doctorService.getDoctors().subscribe(doctors => this.doctors = doctors);
	}

	getPatients(): void {
		this.patientService.getPatients().subscribe(patients => this.patients = patients);
	}

	goBack(): void {
		this.location.back();
	}
	
	checkValues() : boolean {
		if (this.consultation && this.consultation.doctor && this.consultation.patient && 
				this.consultation.description && this.consultation.prescription && this.consultation.appointmentDate) {
			return true;
		} else { 
			return false
		}
	}
	save(): void {
		//TODO fix issue with consultation being null
		/*this.consultation.appointmentDate = new Date().toLocaleDateString();
		if (this.checkValues()){
			this.consultationService.save(this.consultation).subscribe();
		}*/
	}
}

