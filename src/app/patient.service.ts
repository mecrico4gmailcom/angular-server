import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { HandlerService } from "./handler.service";
import server from '../../serverConfig.json'
import { Patient } from './models/patient'

@Injectable({
	providedIn: 'root'
})
export class PatientService {

	httpOptions = {
		headers: new HttpHeaders({ 'Content-Type': 'application/json'})
	};

	private patientsUrl = `${server.baseUrl}${server.patients}`;
	private patientUrl = `${server.baseUrl}${server.patient}`;
	private searchUrl = `${server.baseUrl}${server.patientsSearch}`;

	constructor( 
		private http: HttpClient,
		private handle: HandlerService
	) { }

	/** GET Patient from the server */
	getPatients(): Observable<Patient[]> {
		return this.http.get<Patient[]>(this.patientsUrl).pipe(
			tap(_ => this.handle.log('fetched Patients')),
			catchError(this.handle.error<Patient[]>('getPatients', []))
		);
	}
	
	/** GET Patient by id. Will 404 if id not found 
	 * @param id - name of the operation that failed
	*/
	getPatient(id: string): Observable<Patient> {
		const url = `${this.patientUrl}?id=${id}`;
		return this.http.get<Patient>(url).pipe(
			tap(_ =>{ 
				this.handle.log(`fetched Patient id=${id}`);
				this.handle.log(`used url ${url}`);
			}),
			catchError(this.handle.error<Patient>(`getPatient id=${id}`))
		);
	}

	/** GET Patient by id. Will 404 if id not found 
	 * @param term - name of the operation that failed
	*/
	searchPatients(term: string): Observable<Patient[]> {
		const url = `${this.searchUrl}?term=${term}`;
		return this.http.get<Patient[]>(url).pipe(
			tap(_ =>{ 
				this.handle.log(`fetched Patient term=${term}`);
				this.handle.log(`used url ${url}`);
			}),
			catchError(this.handle.error<Patient[]>(`getPatient term=${term}`))
		);
	}
}
