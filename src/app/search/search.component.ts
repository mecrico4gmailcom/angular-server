import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { DoctorService } from "../doctor.service";
import { PatientService } from "../patient.service";
import { Doctor } from "../models/doctor";
import { Patient } from "../models/patient";
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
	selector: 'app-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
	@Input() source?: string;

	doctors$!: Observable<Doctor[]>;
	patients$!: Observable<Patient[]>;
	
	private doctorTerms = new Subject<string>();
	private patientTerms = new Subject<string>();

	constructor(
		private doctorService: DoctorService,
		private patientService: PatientService
	) {}
	
	ngOnInit(): void {
		if (this.source == 'doctor') {
			this.doctors$ = this.doctorTerms.pipe(
				debounceTime(300),
				distinctUntilChanged(),
				switchMap((term: string) => this.doctorService.searchDoctors(term)),
			);
		} else if (this.source == 'patient') {
			this.patients$ = this.doctorTerms.pipe(
				debounceTime(300),
				distinctUntilChanged(),
				switchMap((term: string) => this.patientService.searchPatients(term)),
			);
		}
	}
		
	search(term: string): void {
		if (this.source == 'doctor') {
			this.doctorTerms.next(term);
		} else if (this.source == 'patient') {
			this.patientTerms.next(term);
		}
	}
}
