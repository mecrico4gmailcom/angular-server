import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HandlerService {

  constructor() { }

  	/** Log a message in the console  
	 * @param message - message to log
	*/
	public log(message: string) {
		console.log(message);
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	*/
	public error<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			console.error(error); // log to console
			this.log(`${operation} failed: ${error.message}`);
			return of(result as T);
		};
	}
}
