import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Patient } from '../models/patient'
import { PatientService } from '../patient.service'

@Component({
  selector: 'app-patient-detail',
  templateUrl: './patient-detail.component.html',
  styleUrls: ['./patient-detail.component.scss']
})
export class PatientDetailComponent implements OnInit {

  patient?: Patient;

	constructor(
		private patientService: PatientService,
		private route: ActivatedRoute,
		private location: Location
	) { }

	ngOnInit(): void {
		this.getPatient();
	}

	getPatient(): void {
		const id = String(this.route.snapshot.paramMap.get('id'));
		this.patientService.getPatient(id).subscribe(patient => this.patient = patient);
	}

	goBack(): void {
		this.location.back();
	}

}
