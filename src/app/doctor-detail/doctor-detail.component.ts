import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Doctor } from '../models/doctor'
import { DoctorService } from '../doctor.service'

@Component({
	selector: 'app-doctor-detail',
	templateUrl: './doctor-detail.component.html',
	styleUrls: ['./doctor-detail.component.scss']
})
export class DoctorDetailComponent implements OnInit {

	doctor?: Doctor;
	constructor(
		private doctorService: DoctorService,
		private route: ActivatedRoute,
		private location: Location
	) { }

	ngOnInit(): void {
		this.getDoctor();
	}

	getDoctor(): void {
		const id = String(this.route.snapshot.paramMap.get('id'));
		this.doctorService.getDoctor(id).subscribe(doctor => this.doctor = doctor);
	}

	goBack(): void {
		this.location.back();
	}
}
