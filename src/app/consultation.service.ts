import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { HandlerService } from "./handler.service";
import { Consultation } from "./models/consultation";
import server from '../../serverConfig.json'

@Injectable({
	providedIn: 'root'
})
export class ConsultationService {

	httpOptions = {
		headers: new HttpHeaders({ 'Content-Type': 'application/json'})
	};

	private consultationsByDoctor = `${server.baseUrl}${server.consultationsByDoctor}`;
	private consultationsByPatient = `${server.baseUrl}${server.consultationsByPatient}`;
	private createUrl = `${server.baseUrl}${server.consultationsCreate}`;


	constructor( 
		private http: HttpClient,
		private handle: HandlerService
	) { }
			
	/** GET Consultations by doctor from the server */
	getDoctorConsultations(id: string): Observable<Consultation[]> {
		const url = `${this.consultationsByDoctor}?id=${id}`;
		return this.http.get<Consultation[]>(url).pipe(
			tap(_ =>{ 
				this.handle.log(`fetched Consutations for Doctor id=${id}`);
				this.handle.log(`used url ${url}`);
			}),
			catchError(this.handle.error<Consultation[]>(`getDoctorConsultations id=${id}`, []))
		);
	}

	/** GET Consultations by doctor from the server */
	getPatientConsultations(id: string): Observable<Consultation[]> {
		const url = `${this.consultationsByPatient}?id=${id}`;
		return this.http.get<Consultation[]>(url).pipe(
			tap(_ =>{ 
				this.handle.log(`fetched Consutations for Patient id=${id}`);
				this.handle.log(`used url ${url}`);
			}),
			catchError(this.handle.error<Consultation[]>(`getPatientConsultations id=${id}`, []))
		);
	}

	/** POST a new Consultation to the server */
	save(consultation: Consultation): Observable<Consultation> {
		return this.http.post<Consultation>(this.createUrl, consultation, this.httpOptions).pipe(
			tap((cons: Consultation) => this.handle.log(`added Consultation w/ id=${cons.id}`)),
			catchError(this.handle.error<Consultation>('addHero'))
		);
	}
}
