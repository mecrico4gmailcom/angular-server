import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Consultation } from "../models/consultation";
import { ConsultationService } from "../consultation.service";

@Component({
	selector: 'app-consultation-list',
	templateUrl: './consultation-list.component.html',
	styleUrls: ['./consultation-list.component.scss']
})
export class ConsultationListComponent implements OnInit {

	@Input() doctorId?: string;
	@Input() patientId?: string;
	consultations: Consultation[] = [];

	constructor(
		private consultationService: ConsultationService,
		private route: ActivatedRoute,
		private location: Location
	) { }

	ngOnInit(): void {
		if (this.doctorId) {
			this.getByDoctor(this.doctorId);
		} else if (this.patientId) {
			this.getByPatient(this.patientId);
		}
		
	}

	getByDoctor(id: string): void {
		this.consultationService.getDoctorConsultations(id).subscribe(
			consultations => this.consultations = consultations
		);
	}

	getByPatient(id: string): void {
		this.consultationService.getPatientConsultations(id).subscribe(
			consultations => this.consultations = consultations
		);
	}

	goBack(): void {
		this.location.back();
	}
}
