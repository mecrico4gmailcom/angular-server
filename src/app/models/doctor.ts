export interface Doctor {
    id: string;
    name: string;
    lastName: string;
    medicalSpecialty: string;
    birthday: Date;
    address: string;
}