export interface Patient {
    id: string;
    name: string;
    lastName: string;
    birthday: Date;
    address: string;
}