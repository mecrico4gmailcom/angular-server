import { Doctor } from "./doctor";
import { Patient } from "./patient";

export interface Consultation {
    id: string;
    description: string;
    appointmentDate: string;
    prescription: string;
    doctor: Doctor;
    patient: Patient;
}