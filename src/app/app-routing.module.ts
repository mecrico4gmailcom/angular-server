import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorListComponent  } from "./doctor-list/doctor-list.component";
import { DoctorDetailComponent } from "./doctor-detail/doctor-detail.component";
import { PatientListComponent  } from "./patient-list/patient-list.component";
import { PatientDetailComponent } from "./patient-detail/patient-detail.component";
import { ConsultationFormComponent } from "./consultation-form/consultation-form.component";

const routes: Routes = [
  { path: 'doctors', component: DoctorListComponent },
  { path: 'doctors/:id', component: DoctorDetailComponent },
  { path: 'patients', component: PatientListComponent },
  { path: 'patients/:id', component: PatientDetailComponent },
  { path: 'consultations/create', component: ConsultationFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
