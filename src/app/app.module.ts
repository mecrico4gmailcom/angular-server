import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DoctorDetailComponent } from './doctor-detail/doctor-detail.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { SearchComponent } from './search/search.component';
import { ConsultationListComponent } from './consultation-list/consultation-list.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientDetailComponent } from './patient-detail/patient-detail.component';
import { ConsultationFormComponent } from './consultation-form/consultation-form.component';

@NgModule({
  declarations: [
    AppComponent,
    DoctorDetailComponent,
    DoctorListComponent,
    SearchComponent,
    ConsultationListComponent,
    PatientListComponent,
    PatientDetailComponent,
    ConsultationFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
