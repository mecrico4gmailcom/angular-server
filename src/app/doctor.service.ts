import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { HandlerService } from "./handler.service";
import server from '../../serverConfig.json'
import { Doctor } from './models/doctor'

@Injectable({
	providedIn: 'root'
})
export class DoctorService {

	httpOptions = {
		headers: new HttpHeaders({ 'Content-Type': 'application/json'})
	};

	private doctorsUrl = `${server.baseUrl}${server.doctors}`;
	private doctorUrl = `${server.baseUrl}${server.doctor}`;
	private searchUrl = `${server.baseUrl}${server.doctorsSearch}`;

	constructor( 
		private http: HttpClient,
		private handle: HandlerService
	) { }

	/** GET doctors from the server */
	getDoctors(): Observable<Doctor[]> {
		return this.http.get<Doctor[]>(this.doctorsUrl).pipe(
			tap(_ => this.handle.log('fetched Doctors')),
			catchError(this.handle.error<Doctor[]>('getDoctors', []))
		);
	}
	
	/** GET Doctor by id. Will 404 if id not found 
	 * @param id - name of the operation that failed
	*/
	getDoctor(id: string): Observable<Doctor> {
		const url = `${this.doctorUrl}?id=${id}`;
		return this.http.get<Doctor>(url).pipe(
			tap(_ =>{ 
				this.handle.log(`fetched Doctor id=${id}`);
				this.handle.log(`used url ${url}`);
			}),
			catchError(this.handle.error<Doctor>(`getDoctor id=${id}`))
		);
	}

	searchDoctors(term : string): Observable<Doctor[]> {
		const url = `${this.searchUrl}?term=${term}`;
		return this.http.get<Doctor[]>(url).pipe(
			tap(_ =>{ 
				this.handle.log(`fetched Doctor term=${term}`);
				this.handle.log(`used url ${url}`);
			}),
			catchError(this.handle.error<Doctor[]>(`getDoctor term=${term}`))
		);
	}
}
